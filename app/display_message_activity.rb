class DisplayMessageActivity < ActionBarActivity

    def onCreate(savedInstanceState)
        super
	setContentView(resources.getIdentifier(activity_display_message, 'layout', 'com.muchmore.myfirstapp'))

        if savedInstanceState == nil
	    getSupportFragmentManager().beginTransaction().add(resources.getIdentifier('container', 'id', 'com.muchmore.myfirstapp'), PlaceholderFragment.new).commit()
	end
    end

    def  onOptionsItemSelected(item)
	id = item.getItemId()
	if id == resources.getIdentifer('action_settings', 'id', 'com.muchmore.myfirstapp')
	    return true
	end
        return super
    end
end
